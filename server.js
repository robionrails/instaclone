require('blueimp-file-upload-node');
var express = require('express');
var amqp    = require('amqplib');
var sockjs  = require('sockjs');
var http    = require('http');
var path    = require('path');
var fs      = require('fs');
var redis   = require('redis');

//redis connection
var DB = redis.createClient();

DB.on("error", function (err) {
    console.log("Error " + err);
});

//rabbitmq connection
var m_conn  = null;

function serve_image(req, res, type) {
    file = req.params.file;
    var img = fs.readFileSync(__dirname + "/uploads/" + type + "/" + file);
    res.writeHead(200, {'Content-Type': 'image/jpg' });
    res.end(img, 'binary');
}

function display_image(msg) {
    var body = msg.content.toString();
    console.log(" [x] Received '%s'", body);
    broadcast(body);
}

function sort_array_by_date(ar) {
    ar.sort(function(a,b){
      return new Date(b.uploaded_at) - new Date(a.uploaded_at);
    });
    return ar;
}

//sockjs clients
var clients = {};

function broadcast(message) {
    var pic = JSON.parse(message);
    for (var i in clients) {
        var clientUser = clients[i].username;
        if(clientUser === 'undefined' || clientUser === pic.user) {
            clients[i].write(message);
        } else {
            DB.smembers('user:'+clientUser+':follows', function(err, reply) {
                if(reply.indexOf(pic.user) !== -1) {
                    clients[i].write(message);
                }
            });
        }
    }
}

var sockjs_opts = {sockjs_url: "http://cdn.sockjs.org/sockjs-0.3.min.js"};
var sockjs_srv = sockjs.createServer(sockjs_opts);

sockjs_srv.on('connection', function(conn) {
    clients[conn.id] = conn;
    
    conn.on('close', function() {
        delete clients[conn.id];
    });

    //setting user in connection
    conn.on('data', function(username) {
        clients[conn.id].username = username === 'undefined' ? undefined : username; 
    });

});

// 2. Express server
var app = express(); /* express.createServer will not work here */
var server = http.createServer(app);

sockjs_srv.installHandlers(server, {prefix:'/instaclone'});

app.configure(function(){
    app.use(express.bodyParser( { keepExtensions: true, uploadDir: __dirname + '/uploads' } ));
    app.use(express.cookieParser());
    app.use(express.session({secret: '1234567890QWERTY'}));
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');

    //add users to locals for login-field
    DB.smembers('users', function(err, reply) {
        app.locals.userlist = reply;
    });
});

// server sends index.html for the homepage
app.get('/', function (req, res) {
    var currentUser = req.cookies.currentUser;
    var currentUserFollows = req.cookies.currentUserFollows;
    var pics = [];

    var render_index = function() {
        //sort pic array before rendering
        pics = sort_array_by_date(pics);

        res.render('index', {
            currentUser: currentUser,
            pics: pics
        });
    }

    var get_pic_data = function(keys, numbOfPics) {
        var actPic = 0;
        keys.forEach(function (key) {
            var user = key.split(':')[1];

            if(currentUser === undefined || currentUser === user || currentUserFollows.indexOf(user) !== -1) {
                DB.hgetall(key, function(err, pic) {
                    pic.user = user;
                    pics.push(pic);

                    actPic++;
                    if(pics.length === numbOfPics || actPic === numbOfPics) {
                        render_index();
                    }
                });
            } else {
                actPic++;
                if (actPic == numbOfPics) {
                    render_index();
                }
            } 
        });
    }

    DB.keys('pic:*', function(err, reply) {
        var numbOfPics = reply.length;
        if (numbOfPics > 0) {
            get_pic_data(reply, numbOfPics);
        } 
        else {
            render_index();
        }
    });
});

//login-path
app.get('/login/:user', function (req, res) {
    res.cookie('currentUser', req.params.user);
    DB.smembers('user:'+req.params.user+':follows', function(err, reply) {
        res.cookie('currentUserFollows', reply);
        res.redirect('/');
    });
});

//logout path
app.get('/logout', function (req, res) {
    res.clearCookie('currentUser');
    res.clearCookie('currentUserFollows');
    res.redirect('/');
});

//follow path
app.get('/:user/follow', function(req, res) {
    var currentUser = req.cookies.currentUser;
    var userToFollow = req.params.user;
    DB.sadd('user:'+ currentUser +':follows', userToFollow, function(err, reply) {
        //actualize follows-list
        DB.smembers('user:'+currentUser+':follows', function(err, reply) {
            res.cookie('currentUserFollows', reply);
            res.redirect('/'+userToFollow);
        });
    });
});

//unfollow path
app.get('/:user/unfollow', function(req, res) {
    var currentUser = req.cookies.currentUser;
    var userToFollow = req.params.user;
    DB.srem('user:'+ currentUser +':follows', userToFollow, function(err, reply) {
        //actualize follows-list
        DB.smembers('user:'+currentUser+':follows', function(err, reply) {
            res.cookie('currentUserFollows', reply);
            res.redirect('/'+userToFollow);
        });
    });
});

//users-path: show all users (for better navigation)
app.get('/users', function(req, res) {
    DB.smembers('users', function(err, reply) {
        res.render('users', {
            currentUser: req.cookies.currentUser,
            users: reply
        });
    });
});

//user-profile-path
app.get('/:user', function(req, res) {
    var user = req.params.user;
    var pics = [];
    var today = new Date();

    var render_profile = function() {
        //sort pic array before rendering
        pics = sort_array_by_date(pics);

        res.render('profiles', {
            currentUser: req.cookies.currentUser,
            currentUserFollows: req.cookies.currentUserFollows,
            user: user,
            pics: pics,
            today: today
        });
    }

    DB.keys('pic:'+user+'*', function(err, reply) {
        var numbOfPics = reply.length;
        if (numbOfPics > 0) {
            reply.forEach(function (key) {
                DB.hgetall(key, function(err, pic) {
                    pics.push(pic);

                    if(pics.length === numbOfPics) {
                        render_profile();
                    }
                });
            });
        } 
        else {
            render_profile();
        }
    });

});

//image-upload-path
app.post('/upload', function(req, res) {
    fs.readFile(req.files.image.path, function (err, data) {
        var imageName = path.basename(req.files.image.path);

        /// If there's an error
        if(!imageName){
            console.log("There was an error");
            res.redirect("/");
        } else {
            var relativePath = "/uploads/fullsize/" + imageName;
            var newPath = __dirname + relativePath;

            /// write file to uploads/fullsize folder
            fs.writeFile(newPath, data, function (err) {
                m_conn.createChannel().then(function(ch) {
                    //save img to redis
                    var currentUser = req.cookies.currentUser;
                    var date = new Date();
                    var key = 'pic:'+ currentUser +':'+ imageName;
                    DB.hmset(key, {"image_name": imageName, "uploaded_at": date, "original": relativePath});

                    var msg = {'img_path': newPath, 'img_name': imageName, 'user': currentUser, 'uploaded_at': date};
                    // resize exchange
                    // empty routing key
                    // JSON message
                    ch.publish('resize', '', new Buffer(JSON.stringify(msg)));
                    ch.close();
                });
                res.redirect("/");
            });
        }
    });
});

// Show files
app.get('/uploads/fullsize/:file', function (req, res){
    serve_image(req, res, "fullsize");
});

app.get('/uploads/thumbs/:file', function (req, res){
    serve_image(req, res, "thumbs");
});


amqp.connect('amqp://localhost').then(function(conn) {
    m_conn = conn;
    return conn.createChannel().then(function(ch) {
        var ex = 'image_ready';
    
        var ok = ch.assertExchange(ex, 'direct', {durable: true});
        ok = ok.then(function() {
            return ch.assertQueue('', {exclusive: true});
        });
    
        ok = ok.then(function(qok) {
            var queue = qok.queue;
            ch.bindQueue(queue, ex, '');
            return queue;
        });

        ok = ok.then(function(queue) {
            return ch.consume(queue, display_image, {noAck: true});
        });
    
        return ok.then(function() {
            // Web Server App
            console.log(' [*] Listening on 0.0.0.0:9999' );
            server.listen(9999, '0.0.0.0');
            return 'started';
        });
    });
}).then(null, console.warn);












