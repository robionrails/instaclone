var redis = require('redis');
var DB = redis.createClient();

DB.on("error", function (err) {
	console.log("Error " + err);
});

var users = [
	"alois",
	"gwendolin",
	"robert",
	"alvaro"
];

//add users
users.forEach(function(user) {
	DB.sadd('users', user);
});
console.log("*) Added users");

//add followers
//schemata:
// users:alice:followers => SET of users
DB.sadd('user:alois:follows','gwendolin');
DB.sadd('user:gwendolin:follows','alois');
DB.sadd('user:gwendolin:follows','robert');
DB.sadd('user:alvaro:follows','alois');
DB.sadd('user:alvaro:follows','gwendolin');
DB.sadd('user:alvaro:follows','robert');

console.log("*) Added follows");

//exit program if all commands are executed
DB.unref();