#!/usr/bin/env ruby
# encoding: utf-8

require 'bunny'
require 'mini_magick'
require 'json'
require "redis"

redis = Redis.new

conn = Bunny.new(:automatically_recover => false)
conn.start

act_dir = File.expand_path(File.dirname(__FILE__))

ch   = conn.create_channel
x_img    = ch.direct('image_ready', :durable => true)
x    = ch.direct('resize', :durable => true)
q    = ch.queue('resize_q', :durable => true).bind(x)

def resize(act_dir, img_data)
  thumb_rel = '/uploads/thumbs/' + img_data['img_name']
	thumb_dir = act_dir + thumb_rel
	path = img_data['img_path']
	image = MiniMagick::Image.open(path)
	image.resize('200x200')
	image.write(thumb_dir)

  return thumb_rel
end

begin
  puts ' [*] Waiting for messages. To exit press CTRL+C'
  q.subscribe(:block => true, :ack => true) do |delivery_info, properties, body|
    puts " [x] Received #{body}"
    img_data = JSON.parse(body)
    
    if thumb = resize(act_dir, img_data)
      #save thumb to redis
      key = "pic:#{img_data['user']}:#{img_data['img_name']}"
      redis.hmset(key, 'thumb', thumb)
      
      #return relative paths for image
      org_path = img_data['img_path']
      img_data['thumb'] = thumb
      img_data['img_path'] = org_path[act_dir.length .. org_path.length]

      #publish that resizing is finished
      x_img.publish(JSON.generate(img_data).to_s)
      ch.acknowledge(delivery_info.delivery_tag, false)
    end
  end
rescue Interrupt => _
  conn.close

  exit(0)
end