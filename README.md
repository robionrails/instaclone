# How to run the project #

Dependencies: Node.js, Ruby

1. Run `npm install`
2. Run `bundle install` (Image resizer is written in ruby)
3. Run `node setup_users.js` (to setup the database with users and followers)
4. Run `ruby resize.rb`
5. Run `node server.js`
6. Open your Browser and go to `http://localhost:9999`

To log in with a user we have created a simple form on the right site in the navigation where you can choose which user you want to be. Every user follows differnt users or maybe no one:

Alois (follows: gwendolin)
Gwendolin (follows: alois, robert)
alvaro (follows: alois, gwendolin, robert)
robert (follows nobody)


# Messaging Project #

The idea is to build an Instagram clone, for some definition of
"clone" and "Instagram".

We want a website where we can upload images, and then images are post
processed by using messaging.

We want to resize images, and once the image is resized, we want to
notify the different type of users about the images being ready.

Image notifications are sent in Real Time to the browser using websockets (with sockjs for example).

## Type of Users ##

- logged in users (registered users)
- anon users
- followers (registered users that follow each others)

## What logged users see ##

Each type of user has its own kind of timeline.

Let's say `Alice` is logged using the website and she follows `Bob`. 

What does Alice sees when she goes to the website?

On her home page, she sees her pictures and Bob's pictures. If Bob uploads a picture, then she sees the picture appearing on her homepage in realtime.

This means on your home page you see your images, plus the images of the people you follow.

Whenever you upload an image, or one of the people you follow uploads an image, you see that image appearing in realtime via websockets.

## What anonymous users see ##

Anonymous users see everybody's pictures appearing on the homepage. They can't upload pictures, and they also can't follow users. 

They also see new uploads in realtime.

## User profiles ##

Whenever I go to some user profile, for example `http://example.com/alice`, I will see Alice's images. If I'm logged in, I can follow Alice.

## Image resize ##

Image resize has to be implemented using RabbitMQ consumers

## Realtime Notifications ##

Notifications has to be implemented via RabbitMQ and websockets.

## Image Storage ##

Images can be stored on disk, but the metadata should be stored on a database, preferably on Redis as explained during the lessons.

## Judgement Criteria ##

The better you implement messaging patterns for this, the more points you get.

If image resizers are implemented in another language, you get extra points. You can implement the website in node.js and the resizer in Ruby.

The more decoupled your application is, the better. For example, you can have one big consumer doing everything, or have separate tasks for notifications, image resize, database updates, and so on.

## Deliverables ##

Please provide the URL to your repo. I would prefer a git repo.